package com.amdocs;

import static org.junit.Assert.*;
import org.junit.Test;

public class IncrementTest {

	@Test
	public void testdecreasecounter1() {
		int k = new Increment().decreasecounter(8);
		assertEquals("Add", 2, k);

	}
	@Test
	public void testdecreasecounter2() {
		int k = new Increment().decreasecounter(0);
		assertEquals("Add", 0, k);

	}
	@Test
	public void testdecreasecounter3() {
		int k = new Increment().decreasecounter(1);
		assertEquals("Add", 1, k);

	}
}

